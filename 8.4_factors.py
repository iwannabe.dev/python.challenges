number = int(input("Enter a positive integer: "))

for iterator in range(number):
    factor = iterator + 1
    if not(number % factor):
        print(f"{factor} is a factor of {number}")
