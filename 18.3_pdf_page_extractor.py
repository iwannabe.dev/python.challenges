import easygui
from PyPDF2 import PdfFileReader, PdfFileWriter

def source_path():
    path = easygui.fileopenbox(
        msg = "Select a PDF file",
        default = "*.pdf"
    )

    if path is None:
        exit()
    else:
        return path


def destination_path():
    path = easygui.filesavebox(
        msg = "Save extracted pages to a file",
        default = "*.pdf"
    )

    if path is None:
        exit()
    else:
        return path

def error_popup(message):
    popup_message = easygui.msgbox(
        msg = message,
        title = "Error"
    )
    return popup_message


def page_no_input(message, label):
    page_no = int(easygui.enterbox(
        msg = message,
        title = label
    ))

    if page_no is None:
        exit()

    while page_no < 1:
        error_popup("Page number must be a positive integer.")
        page_no = page_no_input(message, label)

    return page_no

def extract_and_save_pdf_pages(source_path, destination_path, start_page, end_page):
    pdf_source_file = PdfFileReader(source_file_path)
    new_pdf = PdfFileWriter()

    # for page in pdf_source_file.pages:
    #     page = page.rotateClockwise(angle)
    #     new_pdf.addPage(page)

    for page in pdf_source_file.pages[(start_page - 1) : end_page]:
        new_pdf.addPage(page)

    with open(destination_file_path, "wb") as output_file:
        new_pdf.write(output_file)

if __name__ == '__main__':
    source_file_path = source_path()
    starting_page_no = page_no_input("Please provide a starting page number:", "Starting page number")
    ending_page_no = page_no_input("Please provide an ending page number:", "Ending page number")
    destination_file_path = destination_path()

    while source_file_path == destination_file_path:
        error_popup("Error! Change the filename or directory - you can't override source file.")
        destination_file_path = destination_path()

    extract_and_save_pdf_pages(source_file_path, destination_file_path, starting_page_no, ending_page_no)