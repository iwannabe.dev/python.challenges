import tkinter as tk
import random
from tkinter.filedialog import asksaveasfilename


def save_to_file():
    """Save the poem to a file."""
    filepath = asksaveasfilename(
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")]
    )

    if not filepath:
        return

    with open(filepath, "w") as output_file:
        text = label_generated_poem["text"]
        output_file.write(text)

    window.title(f"Make your own poem! - {filepath}")


def is_first_letter_vowel(word):
    if word[0].lower() in ('a', 'e', 'i', 'o', 'u'):
        return "An"
    else:
        return "A"


def generate_poem():
    nouns = entry_nouns.get().split(", ")
    noun1, noun2, noun3 = random.sample(nouns, 3)

    verbs = entry_verbs.get().split(", ")
    verb1, verb2, verb3 = random.sample(verbs, 3)

    adjectives = entry_adjectives.get().split(", ")
    adjective1, adjective2, adjective3 = random.sample(adjectives, 3)

    prepositions = entry_prepositions.get().split(", ")
    preposition1, preposition2, preposition3 = random.sample(prepositions, 3)

    adverbs = entry_adverbs.get().split(", ")
    adverb1 = random.sample(adverbs, 1)[0]

    article = is_first_letter_vowel(adjective1)

    poem = (
        f"{article} {adjective1} {noun1}\n\n"
        f"A {adjective1} {noun1} {verb1} {preposition1} the {adjective2} {noun2}\n"
        f"{adverb1}, the {noun1} {verb2}\n"
        f"the {noun2} {verb3} {preposition2} a {adjective3} {noun3}"
    )

    label_generated_poem["text"] = poem

window = tk.Tk()
window.title("Make your own poem!")
window.rowconfigure(index=3, weight=1)
window.columnconfigure(index=0, weight=1)


# 1st row of grid
label_main = tk.Label(text="Enter your favourite words, separated by commas.")
label_main.grid(row=0, column=0, pady=10)


# 2nd row of grid
frame_inputs = tk.Frame(master=window)
frame_inputs.columnconfigure(index=1, weight=1, minsize=500)
frame_inputs.grid(row=1, column=0, padx= 5, sticky="ew")

label_nouns = tk.Label(master=frame_inputs, text="Nouns:")
label_nouns.grid(row=0, column=0, sticky="e")

label_verbs = tk.Label(master=frame_inputs, text="Verbs:")
label_verbs.grid(row=1, column=0, sticky="e")

label_adjectives = tk.Label(master=frame_inputs, text="Adjectives:")
label_adjectives.grid(row=2, column=0, sticky="e")

label_prepositions = tk.Label(master=frame_inputs, text="Prepositions:")
label_prepositions.grid(row=3, column=0, sticky="e")

label_adverbs = tk.Label(master=frame_inputs, text="Adverbs:")
label_adverbs.grid(row=4, column=0, sticky="e")

entry_nouns = tk.Entry(master=frame_inputs)
entry_nouns.grid(row=0, column=1, sticky="nsew")

entry_verbs = tk.Entry(master=frame_inputs)
entry_verbs.grid(row=1, column=1, sticky="nsew")

entry_adjectives = tk.Entry(master=frame_inputs)
entry_adjectives.grid(row=2, column=1, sticky="nsew")

entry_prepositions = tk.Entry(master=frame_inputs)
entry_prepositions.grid(row=3, column=1, sticky="nsew")

entry_adverbs = tk.Entry(master=frame_inputs)
entry_adverbs.grid(row=4, column=1, sticky="nsew")


# 3rd row of grid
button_generate = tk.Button(
    master=window, text="Generate", relief=tk.GROOVE, border=3,
    command=generate_poem
)
button_generate.grid(row=2, column=0, pady=10)


# 4th row of grid
frame_poem = tk.Frame(master=window, relief=tk.GROOVE, border=4)
frame_poem.grid(row=3, column=0, sticky="nsew", padx=5, pady=5)

label_poem_heading = tk.Label(master=frame_poem, text="Your poem:")
label_poem_heading.pack(padx=10, pady=10)

label_generated_poem = tk.Label(
    master=frame_poem,
    text="Press the 'Generate' button to generate your own poem."
)
label_generated_poem.pack(padx=10, pady=10)

button_save = tk.Button(master=frame_poem, text="Save to file", command=save_to_file)
button_save.pack(padx=10, pady=10)

window.mainloop()