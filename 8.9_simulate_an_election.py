import random

def elect_with_set_probability(probability_A_wins):
    if random.random() < probability_A_wins:
        return 'A'
    else:
        return 'B'

# Chance of A winning election (from a poll)
region_1_probability_A_wins = 0.87
region_2_probability_A_wins = 0.65
region_3_probability_A_wins = 0.17

A_wins = 0
B_wins = 0

for trial in range(10_000):
    votes_for_A = 0
    votes_for_B = 0
    
    if elect_with_set_probability(region_1_probability_A_wins) == 'A':
        votes_for_A += 1
    else:
        votes_for_B += 1
        
    if elect_with_set_probability(region_2_probability_A_wins) == 'A':
        votes_for_A += 1
    else:
        votes_for_B += 1
        
    if elect_with_set_probability(region_3_probability_A_wins) == 'A':
        votes_for_A += 1
    else:
        votes_for_B += 1

    if votes_for_A > votes_for_B:
        A_wins += 1
    else:
        B_wins += 1

A_wins_probability = (A_wins / 10_000) * 100
B_wins_probability = (B_wins / 10_000) * 100

print(f"Probability of A winning election: {A_wins_probability:.2f}%")
print(f"Probability of B winning election: {B_wins_probability:.2f}%")
