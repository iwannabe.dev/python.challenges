import random

def generate_random_words(list_of_words, qty):
    generated_words = [None] * qty
    generated_words = random.sample(list_of_words, qty)
    return generated_words

def is_first_letter_vowel(word):
    if word[0].lower() in ('a', 'e', 'i', 'o', 'u'):
        return "An"
    else:
        return "A"

def generate_poem():
    poem = (
        f"{article} {adj1} {noun1}\n\n"
        f"{article} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\n"
        f"{adverb1}, the {noun1} {verb2}\n"
        f"the {noun2} {verb3} {prep2} a {adj3} {noun3}"
    )
    return poem
    

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert",
         "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes",
         "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant",
              "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in",
                "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously",
           "sensuously"]

noun1, noun2, noun3 = generate_random_words(nouns, 3)
verb1, verb2, verb3 = generate_random_words(verbs, 3)
adj1, adj2, adj3 = generate_random_words(adjectives, 3)
prep1, prep2 = generate_random_words(prepositions, 2)

adverb1 = ""
adverb1 = adverb1.join(generate_random_words(adverbs, 1))

article = is_first_letter_vowel(adj1)

print(generate_poem())
