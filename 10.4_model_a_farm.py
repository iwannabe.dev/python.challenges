class Animal:
    position = [0, 0]

    def __init__(self, name, weight, age):
        self.name = name
        self.weight = weight
        self.age = age

    def move(self, new_position, type_of_movement="walked"):
        previous_position = self.position
        self.position = new_position
        self.type_of_movement = type_of_movement
        return f"{self.name} {type_of_movement} from position {previous_position} to {self.position}"

    def speak(self, sound="Hi!"):
        return f"{self.name} says {sound}"

class Fowl(Animal):
    eggs = 0
    
    def lay_eggs(self, no_of_new_eggs):
        self.eggs += no_of_new_eggs
        return f"{self.name} laid {no_of_new_eggs} new eggs and has {self.eggs} in total"


class FowlThatCanFly(Fowl):
    def move(self, new_position, type_of_movement="flew"):
        return super().move(new_position, type_of_movement)


class Chicken(Fowl):
    def speak(self, sound="cheep"):
        return super().speak(sound)


class Goose(FowlThatCanFly):
    def speak(self, sound="honk"):
        return super().speak(sound)


class Duck(FowlThatCanFly):
    def speak(self, sound="quack"):
        return super().speak(sound)


class Pig(Animal):
    def speak(self, sound="oink"):
        return super().speak(sound)


class Cow(Animal):
    def speak(self, sound="moo"):
        return super().speak(sound)


cow = Cow("Betty", 300, 3)
pig = Pig("Rupert", 70, 1)
duck = Duck("Rose", 3, 1)
goose = Goose("Emilly", 3, 2)
chicken = Chicken("Anna", 2, 1)

print("On our farm we have few different animals: ")
print(f"Cow {cow.name} who is {cow.age} years old and weight {cow.weight}kg")
print(f"Pig {pig.name} which is {pig.age} years old and weight {pig.weight}kg")
print(f"Duck {duck.name} which is {duck.age} years old and weight {duck.weight}kg")
print(f"Goose {goose.name} which is {goose.age} years old and weight {goose.weight}kg")
print(f"Chicken {chicken.name} which is {chicken.age} years old and weight {chicken.weight}kg")

print("\nThey are all very friendly and always greet everyone: ")
print(cow.speak())
print(pig.speak())
print(duck.speak())
print(goose.speak())
print(chicken.speak())

print("\nThey love walking around our farm: ")
print(cow.move([7, 3]))
print(pig.move([3, 6]))
print(chicken.move([1, 7]))

print("\n...or flying ...if they can: ")
print(duck.move([3, 4]))
print(goose.move([8, 9]))


