def convert_cel_to_far(temp_cel):
    """Returns temperature in Fahrenheit for a given degrees Celsius"""
    temp_far = temp_cel * 9/5 + 32
    return temp_far

def convert_far_to_cel(temp_far):
    """Returns temperature in Celsius for a given degrees Fahrenheit"""
    temp_cel = (temp_far - 32) * 5/9
    return temp_cel

user_input_1 = input("Enter a temperature in degrees F: ")
temp_far = float(user_input_1)
temp_cel = convert_far_to_cel(temp_far)
print(f"{temp_far} degrees F = {temp_cel:.2f} degrees C\n")

user_input_2 = input("Enter a temperature in degrees C: ")
temp_cel = float(user_input_2)
temp_far = convert_cel_to_far(temp_cel)
print(f"{temp_cel} degrees C = {temp_far:.2f} degrees F")
