import random

def coin_flip():
    """Randomly return 'heads' or 'tails'."""
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"

flips_counter = 0

for trial in range(10_000):
    first_flip = coin_flip()
    flips_counter += 1

    while coin_flip() == first_flip:
        flips_counter += 1

    flips_counter += 1

avg_flips_per_trial = flips_counter / 10_000

print(f"It took {avg_flips_per_trial} flips on average per each trial to complete a sequence containing at least one 'head' and one 'tail'")
