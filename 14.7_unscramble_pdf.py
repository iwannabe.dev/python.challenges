# Solution to a challenge 14.7: 'Unscramble a PDF' from a
# "Python Basics A Practical Introduction to Python 3" book

from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path

# location of source PDF
source_pdf_path = (
    Path.home() /
    "Za raczke" /
    "python.challenges" /
    "practice_files" /
    "scrambled.pdf"
)

pdf_reader = PdfFileReader(str(source_pdf_path))
num_pages = pdf_reader.getNumPages()
pages = [None] * num_pages

for page in pdf_reader.pages:
    text = page.extractText()
    rotation_degrees = page["/Rotate"]
    if rotation_degrees != 0:
        page.rotateCounterClockwise(rotation_degrees)    
    pages[int(text) - 1] = page

pdf_writer = PdfFileWriter()
for page in pages:
    pdf_writer.addPage(page)

output_pdf_path = Path.home() / "unscrambled.pdf"
with Path(output_pdf_path).open(mode="wb") as output_file:
    pdf_writer.write(output_file)
