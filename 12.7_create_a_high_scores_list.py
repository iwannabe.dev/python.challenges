import pathlib
import csv

def get_highest_scores_only(scores):
    high_scores = {}
    for player in scores:
        player_name = player["name"]
        player_score = int(player["score"])
        if player_name not in high_scores:
            high_scores[player_name] = player_score
        else:
            if player_score > high_scores[player_name]:
                high_scores[player_name] = player_score
    return high_scores
    

source_file_path = pathlib.Path.home() / "practice_files" / "scores.csv"
destination_file_path = pathlib.Path.home() / "practice_files" / "high_scores.csv"
scores = []

with source_file_path.open(mode="r", encoding="utf-8") as source_file:
    reader = csv.DictReader(source_file)
    for row in reader:
        scores.append(row)

high_scores = get_highest_scores_only(scores)

with destination_file_path.open(mode="w", encoding="utf-8", newline="") as destination_file:
    writer = csv.DictWriter(destination_file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for player in high_scores:
        player_dict = {"name": player, "high_score": high_scores[player]}
        writer.writerow(player_dict)

