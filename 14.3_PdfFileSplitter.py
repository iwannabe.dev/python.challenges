# Solution to a challenge 14.3: 'PdfFileSplitter Class' from a
# "Python Basics A Practical Introduction to Python 3" book

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

class PdfFileSplitter:
    def __init__(self, input_file_name: str):
        self.pdf_reader = PdfFileReader(input_file_name)
        self.pdf_writer1 = PdfFileWriter()
        self.pdf_writer2 = PdfFileWriter()
    
    def split(self, split_point: int):
        for n in range(split_point):
            page = self.pdf_reader.getPage(n)
            self.pdf_writer1.addPage(page)
        total_pages = self.pdf_reader.getNumPages()
        for n in range(split_point, total_pages):
            page = self.pdf_reader.getPage(n)
            self.pdf_writer2.addPage(page)
        
    def write(self, output_file_name: str):
        with Path(output_file_name + "_1.pdf").open(mode="wb") as output_file:
            self.pdf_writer1.write(output_file)
        with Path(output_file_name + "_2.pdf").open(mode="wb") as output_file:
            self.pdf_writer2.write(output_file)


pdf_splitter = PdfFileSplitter("mydoc.pdf")
pdf_splitter.split(4)
pdf_splitter.write("mydoc_split")
