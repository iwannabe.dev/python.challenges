def enrollment_stats(universities):
    total_students = []
    total_tuition = []
    for university in universities:
        total_students.append(university[1])
        total_tuition.append(university[2])
    return total_students, total_tuition

def mean(values):
    return sum(values) / len(values)

def median(values):
    values.sort()
    if len(values) % 2 == 1:
        index_of_median = len(values) // 2
        return values[index_of_median]
    else:
        value_at_index_left_to_median = values[(len(values) - 1) // 2]
        value_at_index_right_to_median = values[(len(values) + 1) // 2]
        return mean([value_at_index_left_to_median, value_at_index_right_to_median])

universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

total_students, total_tuition = (enrollment_stats(universities))

print("***************************")
print(f"Total students: {sum(total_students):,}")
print(f"Total tuition: $ {sum(total_tuition):,} \n")
print(f"Student mean: {mean(total_students):,.2f}")
print(f"Student media: {median(total_students):,} \n")
print(f"Tuition mean: $ {mean(total_tuition):,.2f}")    
print(f"Tuition media: $ {median(total_tuition):,}")
print("***************************")
