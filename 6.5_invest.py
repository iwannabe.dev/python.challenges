# 6.5 Challenge: Track Your Investments

def invest(amount, rate, years):
    """Calculates and prints out an earnings on investment for specified
    principal amount, at specified annual rate of return over specified number
    of years"""
    for year in range(1, years + 1):
        earnings_per_year = amount * rate
        amount += earnings_per_year
        print(f"year {year}: ${amount:,.2f}")

amount = float(input("Enter an initial amount: "))
rate = float(input("Enter an annual percentage rate: "))
years = int(input("Enter a number of years: "))

invest(amount, rate, years)
