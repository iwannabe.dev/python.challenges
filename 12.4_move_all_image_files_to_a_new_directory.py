import pathlib

practice_files_dir = pathlib.Path.home() / "practice_files"

images_dir = practice_files_dir / "images"
images_dir.mkdir()

documents_dir = practice_files_dir / "documents"

for path in documents_dir.glob("image*"):
    if path.suffix.lower() in [".png", ".jpg", ".gif"]:
        path.replace(images_dir / path.name)
