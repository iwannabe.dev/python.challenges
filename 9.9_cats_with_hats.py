cats_with_hats = []

for lap in range(1, 101):
    for cat in range(1, 101):
        if cat % lap == 0:
            if cat in cats_with_hats:
                cats_with_hats.remove(cat)
            else:
                cats_with_hats.append(cat)

print("Cats with hats: ")        
print(cats_with_hats)
