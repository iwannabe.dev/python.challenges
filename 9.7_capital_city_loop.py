import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}


state, capital = random.choice(list(capitals_dict.items()))

while True:
    answer = input(f"Enter the capital of {state}: ").lower()
    if answer == "exit":
        print(f"The capital of {state} is {capital}.")
        print("Goodbye.")
        break
    elif answer == capital.lower():
        print("Correct!")
        break

